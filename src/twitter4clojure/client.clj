(ns twitter4clojure.client
  (:require [clojure.data.json :as json]
            [clojure.string :as string]
            [oauth.client :as oauthclient]))

(defprotocol System
  ""
  (shutdown [this] "シャットダウンします。"))


(defprotocol TwitterBase
  ""
  (screen-name [this] "スクリーンネームを返します。")
  (user-id [this] "ユーザーIDを返します。")
  (add-rate-limit-status-hook [this hook-fn] "レートリミットがあれした時に呼び出される関数を追加します。")
  (authorization [this] "ユーザー情報を返します。")
  (configuration [this] "設定情報を返します。"))
